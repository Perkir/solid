namespace SOLID.Dependency_Inversion
{
    public class SmsNotifier: INotifier
    {
        public void Notify(string message)
        {
            SendSms(message);
        }

        private void SendSms(string message)
        {
            message = "SMS sended";
        }
    }
}