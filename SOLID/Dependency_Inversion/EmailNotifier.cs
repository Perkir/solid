namespace SOLID.Dependency_Inversion
{
    public class EmailNotifier: INotifier
    {
        public void Notify( string message)
        {
            SendEmail(message);
        }
        private void SendEmail(string message)
        {
            message = "Email sended";
        }
    }
}