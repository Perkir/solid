namespace SOLID.Dependency_Inversion
{
    public class TeamMemberManager
    {
        private readonly INotifier _notifier;
        
        public TeamMemberManager(INotifier notify)
        {
            this._notifier = notify;
        }

        public void ChangePassword(string userName, string oldPassword, string newPassword)
        {
            
            _notifier.Notify("Password has changed");
        }
    }
}