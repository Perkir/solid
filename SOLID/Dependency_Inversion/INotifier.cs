namespace SOLID.Dependency_Inversion
{
    public interface INotifier
    {
        void Notify( string message);
    }
}