namespace SOLID.Single_Responsibility
{
    public class Team
    {
        public string TeamId { get; set; }
        public string TeamName { get; set; }
    }
}