using System.Linq;

namespace SOLID.Single_Responsibility
{
    public class TeamSearch
    {
        private readonly ITeamRepository _repository;

        public TeamSearch(ITeamRepository repository)
        {
            _repository = repository;
        }

        public ITeamRepository SearchById(string value)
        {
            return (ITeamRepository)_repository.Teams.FirstOrDefault(x => x.TeamId == value);
        }

        public ITeamRepository SeachByName(string value)
        {
            return (ITeamRepository) _repository.Teams.FirstOrDefault(x => x.TeamName == value);
        }
    }
}