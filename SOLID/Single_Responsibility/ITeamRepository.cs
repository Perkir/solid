using System.Linq;

namespace SOLID.Single_Responsibility
{
    public interface ITeamRepository
    {
        IQueryable<Team> Teams { get; set; }
    }
}