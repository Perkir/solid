namespace SOLID.Interface_Segregation
{
    public interface IKanbanProcess
    {
        void ProcessKanban();
        int GetKanbanMetrics();
    }
}