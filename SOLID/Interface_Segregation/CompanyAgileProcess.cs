namespace SOLID.Interface_Segregation
{
    public class CompanyAgileProcess: IKanbanProcess, IScrumProcess

    {
        public void ProcessKanban()
        {
            var startProcess = true;
        }

        public int GetKanbanMetrics()
        {
            return 100;
        }

        public void ProcessScrum()
        {
            var startProcess = true;
        }

        public int GetScrumMetrics()
        {
            return 200;
        }
    }
}