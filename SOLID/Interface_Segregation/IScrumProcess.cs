namespace SOLID.Interface_Segregation
{
    public interface IScrumProcess
    {
        void ProcessScrum();
        int GetScrumMetrics();
    }
}