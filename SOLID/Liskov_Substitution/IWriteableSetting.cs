namespace SOLID.Liskov_Substitution
{
    public interface IWriteableSetting
    {
        void SetSetting(string settingName, string settingValue);
    }
}