namespace SOLID.Liskov_Substitution
{
    public interface IReadableSetting
    {
        IReadableSetting GetSetting();
    }
}