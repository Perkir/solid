﻿using System;
using SOLID.Dependency_Inversion;
using SOLID.Interface_Segregation;
using SOLID.Liskov_Substitution;
using SOLID.Open_Closed;
using SOLID.Single_Responsibility;

namespace SOLID
{
    internal static class Program
    {
        static void Main(string[] args)
        {
            // SR
            var team = new Team {TeamId = "1", TeamName = "test"};
            var searchTeam = new TeamSearch(null);
            searchTeam.SearchById("1");
            searchTeam.SeachByName("test");
            
            // OC
            var metrics = new VelocityCalculator();
            metrics.Calculate(null);
            
            // LS
            var agileTeam = new AgileTeamSettings();
            var otherTeam = new OtherTeamSettings();
            agileTeam.SetSetting("value", "123");
            agileTeam.GetSetting();
            otherTeam.GetSetting();
            
            // IS
            var companyAgile = new CompanyAgileProcess();
            companyAgile.ProcessKanban();
            companyAgile.ProcessScrum();
            companyAgile.GetKanbanMetrics();
            companyAgile.GetScrumMetrics();

            // DI
            INotifier notifier = null;

            if ("TeamMemberHasEmail" == "TeamMemberHasEmail")
            {
                notifier = new EmailNotifier();
            }

            if ("TeamMemberHasPhoneNumber" == "TeamMemberHasPhoneNumber")
            {
                notifier = new SmsNotifier();
            }

            TeamMemberManager manager = new TeamMemberManager(notifier);
            manager.ChangePassword("user", "old", "new");
        }
    }
}