namespace SOLID.Open_Closed
{
    public class MetricCalculatorForKanban :IMetricCalculator
    {
       public int CycleTime { get; set; }
       public int Throughput { get; set; }
       
        public decimal CalculateMetric()
        {
            return CycleTime * Throughput;
        }
    }
}