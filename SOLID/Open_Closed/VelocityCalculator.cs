namespace SOLID.Open_Closed
{
    public class VelocityCalculator
    {
        public decimal Calculate(IMetricCalculator obj)
        {
            decimal velocity = obj.CalculateMetric();
            return velocity;
        }
    }
}