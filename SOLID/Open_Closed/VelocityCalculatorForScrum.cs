namespace SOLID.Open_Closed
{
    public class MetricCalculatorForScrum: IMetricCalculator
    {
        public int Velocity { get; set; }
        public bool SprintGoalSuccessRates { get; set; }
        public int Defects { get; set; }

        public decimal CalculateMetric()
        {
            if (SprintGoalSuccessRates)
            {
                return Velocity / Defects;
            }
            else
            {
                return Velocity / Defects - 10;
            }
        }
    }
}