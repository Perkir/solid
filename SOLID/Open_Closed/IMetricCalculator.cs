namespace SOLID.Open_Closed
{
    public interface IMetricCalculator
    {
        decimal CalculateMetric();
    }
}